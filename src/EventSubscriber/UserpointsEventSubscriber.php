<?php
namespace Drupal\term_rank\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Psr\Log\LoggerInterface;
use Drupal\transaction\Event\TransactionExecutionEvent;

/**
 * Class EasyWeChatAuthmapSubscriber.
 */
class UserpointsEventSubscriber implements EventSubscriberInterface {
  
  /**
   * @var LoggerInterface
   */
  private $logger;
  
  /**
   * 
   * @var \Drupal\term_rank\PointsOperatorServiceInterface $points_operator
   */
  private $points_operator;
  
  /**
   *
   * @var \Drupal\term_rank\RankOperatorServiceInterface $rank_operator
   */
  private $rank_operator;
  
  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[TransactionExecutionEvent::EVENT_NAME][] = array('updatePoints');
    return $events;
  }
  
  public function __construct() {
    $this->logger = \Drupal::service('logger.factory')->get('term_ranks');
    $this->points_operator = \Drupal::service("term_rank.points_operator");
    $this->rank_operator = \Drupal::service("term_rank.rank_operator");
  }
  
  /**
   * 
   * @param \Drupal\transaction\Event\TransactionExecutionEvent $event
   */
  function updatePoints($event) {
    
    $trans  = $event->getTransaction();
    $entity = $trans->getTargetEntity();
    if ($entity->getEntityTypeId() == 'user') {
       $this->points_operator->update($entity, $trans, TRUE);
       //$this->rank_operator->update($entity, TRUE);
    }
    
  }
}
