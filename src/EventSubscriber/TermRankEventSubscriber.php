<?php
namespace Drupal\term_rank\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Psr\Log\LoggerInterface;

/**
 * Class EasyWeChatAuthmapSubscriber.
 */
class TermRankEventSubscriber implements EventSubscriberInterface {
  
  /**
   * @var LoggerInterface
   */
  private $logger;
  
  /**
   * 
   * @var \Drupal\term_rank\RankOperatorService $operator
   */
  private $operator;
  
  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //$events[TransactionExecutionEvent::EVENT_NAME][] = array('updateRank');
    //return $events;
  }
  
  public function __construct() {
    $this->logger = \Drupal::service('logger.factory')->get('term_ranks');
    $this->operator = \Drupal::service("term_rank.rank_operator");
  }
  
  /**
   * 
   * @param \Drupal\transaction\Event\TransactionExecutionEvent $event
   */
  function updateRank($event) {
    
    $trans = $event->getTransaction();
    $entity = $trans->getTargetEntity();
    if ($entity->getEntityTypeId() == 'user') {
       $this->operator->update($entity);
    }
    
  }
}
