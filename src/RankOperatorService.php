<?php

namespace Drupal\term_rank;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Implementation of a Drupal service.
 *
 * This service class will provide all the business logic of this module.
 */
class RankOperatorService implements RankOperatorServiceInterface {
  
  /**
   * The Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;
  
  /**
   * 
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $ranksettings;
  
  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
    $this->ranksettings = \Drupal::config('term_rank.settings');
  }

  public function update($user, $save_immediate = FALSE) {
    //default user rank is 0
    $rank_tid  = empty($user->field_rank->target_id)  ? 0 : $user->field_rank->target_id;
    $point     = $user->field_total_points->value;
    
    $ranks = $this->getUserRanks($point);
    $rank_new = $ranks['rank'];
    if ($rank_new != $rank_tid) {
      $user->field_rank->setValue(['target_id' => $rank_new]);
      if ($save_immediate == TRUE) {
        $user->save();
      }
    }
    
  }
  
  /**
   * Get current rank and the next rank
   * @param int $userpoint
   * @return array array('rank' => current, 'next' => next_rank)
   */
  public function getUserRanks($userpoint) {
    $tids = $this->getTaxonomyTermsSortedByWeight();
    
    $next = $rank = 0;
    
    foreach ($tids as $tid) {
      $term = \Drupal\taxonomy\Entity\Term::load($tid);
      if ($userpoint < $term->field_points->value) {
        $next = $term->id();
        break;
      }
      else {
        $rank = $term->id();
      }
    }
    
    //when next is equal to current, it is the last level
    $next = $next == 0 ? $rank : $next;
    return ['rank' => $rank, 'next' => $next];
  }
  
  
  /**
   * Get taxonomy terms sorted by weight.
   *
   * @param int $vid
   *   The vocabulary id.
   *
   * @return array
   *   Returns an array of term id | name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTaxonomyTermsSortedByWeight($vid = null) {
    $vid =  (null == $vid) ? $this->ranksettings->get('vocabulary_id') : $vid;
    
    // Initialize the items.
    $items = [];
    
    // Get the term storage.
    $entity_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    
    // Query the terms sorted by weight.
    $query_result = $entity_storage->getQuery()
    ->condition('vid', $vid)
    ->sort('weight', 'ASC')
    ->execute();
    
    return $query_result;
    // Load the terms.
    //$terms = $entity_storage->loadMultiple($query_result);
    
    /**
    foreach ($terms as $term) {
      $items[$term->id()] = $term->getName();
    }
    
    // Return the items.
    return $items;
    **/
  }

}