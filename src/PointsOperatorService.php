<?php

namespace Drupal\term_rank;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Implementation of a Drupal service.
 *
 * This service class will provide all the business logic of this module.
 */
class PointsOperatorService implements PointsOperatorServiceInterface {
  
  /**
   * The Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;
  
  /**
   * 
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $ranksettings;
  
  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
    $this->ranksettings = \Drupal::config('term_rank.settings');
  }
  
  public function update($user, $transaction, $save_immediate = FALSE) {
    
    $settings = $transaction->getType()->getPluginSettings();
    $amount   = $transaction->get($settings['amount'])->value;
    
    $only_increasing = $this->ranksettings->get('only_increase_points');
    if ( ( $only_increasing == FALSE && $amount < 0)
      || ( $only_increasing == TRUE  && $amount > 0)){
      
       $current = $user->field_total_points->value;
       if ($current < 0 || !is_numeric($current)) {
         $current = 0;
       }
       $user->field_total_points->setValue($current + $amount);
       if ($save_immediate == TRUE) {
         $user->save();
       }
       return $user;
    }
  }

}