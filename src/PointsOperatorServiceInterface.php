<?php

namespace Drupal\term_rank;

interface  PointsOperatorServiceInterface {
  
  
  /**
   * 
   * @param \Drupal\user\Entity\User $user
   * @param \Drupal\transaction\Entity\Transaction $transaction
   */
  function update($user, $transaction, $save_immediate = FALSE);
  
}