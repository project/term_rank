<?php

namespace Drupal\term_rank;

interface  RankOperatorServiceInterface {
  
  
  /**
   * 
   * @param \Drupal\user\Entity\User $user
   */
  function update($user, $save_immediate = FALSE);
  
}