<?php

namespace Drupal\term_rank\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\user\UserInterface;

/**
 * Provides "Unblock User" action.
 *
 * @RulesAction(
 *   id = "rules_user_rank",
 *   label = @Translation("Rerank a user"),
 *   category = @Translation("User"),
 *   context = {
 *     "user" = @ContextDefinition("entity:user",
 *       label = @Translation("User"),
 *       description = @Translation("Specifies the user, that should be rank updates.")
 *     ),
 *   }
 * )
 *
 */
class UserRank extends RulesActionBase {
  /**
   * Flag that indicates if the entity should be auto-saved later.
   *
   * @var bool
   */
  protected $saveLater = FALSE;
  
  /**
   * Unblock a user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to unblock.
   */
  protected function doExecute(UserInterface $user) {
    \Drupal::service("term_rank.rank_operator")->update($user);
  }
  
  /**
   * {@inheritdoc}
   */
  public function autoSaveContext() {
    /**
    if ($this->saveLater) {
      return ['user'];
    }
    return [];
    **/
  }
  
}
