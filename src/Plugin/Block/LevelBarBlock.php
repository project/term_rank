<?php

namespace Drupal\term_rank\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Provides a Level bar block.
 *
 * @Block(
 *   id = "level_bar_block",
 *   admin_label = @Translation("Level bar block"),
 *   category = @Translation("Term rank"),
 * )
 */
class LevelBarBlock extends BlockBase {
  
  /**
   * {@inheritdoc}
   */
  public function build() {
   /**
    * 
    * @var \Drupal\term_rank\RankOperatorService $operator
    */
    $operator     =  \Drupal::service("term_rank.rank_operator");
    $term_result  = $operator->getTaxonomyTermsSortedByWeight();
    $terms        = \Drupal::entityTypeManager()->getStorage("taxonomy_term")->loadMultiple($term_result);
   
    $userpoints = 20;
    $current    = $operator->getUserRanks($userpoints);
    foreach ($terms as $term) {
      $levels[$term->id()] = ['id' => $term->id(), 'name' => $term->getName(), 'points' => $term->field_points->value ];
    }
   
   if ($levels[$current['next']]['points'] <> $levels[$current['rank']]['points']) {
     $percentage = ($userpoints - (int) $levels[$current['rank']]['points'])/((int)$levels[$current['next']]['points'] - (int)$levels[$current['rank']]['points']);
     $percentage = 100*number_format($percentage,2);
   }
   else {
     $percentage = 100;
   }
   
   $current = ['points' => $userpoints, 'percentage' => $percentage] + $current;
   
   $module_handler = \Drupal::service('module_handler');
   $module_path = $module_handler->getModule('term_rank')->getPath();
   
   $current['module_path'] = base_path() .  $module_path; 
   
   return [
      '#theme'   => 'level_bar',
      '#levels'  => $levels,
      '#current' => $current,
      '#attached' => ['library' =>  ['term_rank/term_rank.bar']]
    ];
  }
  
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['user']);
  }
  
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), \Drupal::entityTypeManager()->getDefinition("taxonomy_term")->getListCacheTags());
  }
  
}