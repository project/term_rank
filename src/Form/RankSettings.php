<?php

namespace Drupal\term_rank\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RankSettings.
 */
class RankSettings extends ConfigFormBase {
  
  protected function getEditableConfigNames() {
    return [
      'term_rank.settings',
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'term_rank_settings_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $form['vocabulary_id'] = [
      '#type'     => 'select',
      '#multiple' => FALSE,
      '#title'    => $this->t('Vocabulary id'),
      '#options'  => taxonomy_vocabulary_get_names(),
      '#size'     => 5,
      '#description'   => $this->t('The vocabulary id of your rank levels.'),
      '#default_value' => $config->get('vocabulary_id'),
    ];
    $form['only_increase_points'] = [
      '#title' => $this->t('Only sync increasing points'),
      '#type'  => 'checkbox',
      '#default_value' => $config->get('only_increase_points'),
      '#description'   => $this->t('Checked this box if you want to update the total points only increasing the points'),
    ];
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    
    $this->config($this->getEditableConfigNames()[0])
    ->set('only_increase_points', $form_state->getValue('only_increase_points'))
    ->set('vocabulary_id', $form_state->getValue('vocabulary_id'))
    ->save();
  }
  
}