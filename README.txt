This project provides a user rank/level functionality via taxonomy term.
You can use a vocabulary to define a ranks set, and the user rank will be updated according to the userpoint and the rank meets.


## STRUCTURE:

1. A vocabulary named rank
   the term has a points field, which will be the minimal points of this level.

2. User will have a total_points field, which will be updated automatically when the userpoint transaction is executed

3. User will have a field_rank field which will refer to the rank vocabulary
   This rank will be updated automatically when user is saved and the total_points is updated
   
4. To provide a level bar to indicate the user's level, you can place it in any region and any page.


## INFORMATION:

1. Term weight is the level order, and the label is the level name
   so please set the order of the ranks firstly, set the weight of the terms, and save weight.(Mandatory)

2. The total_points of user could be updated only when the user gains the points, and won't updated when user loses points.
   Becase in the most case, the users' level only goes up.
  
3. You can edit the total_points (reset) in anytime, the user's rank will be update (reset).

   
## INSTALL:

1. Enable this module

2. [Optional] Enable user point default module

3. Go to admin/config/people/accounts/fields, make rank and total_points field visible on form page.

4. Go to admin/structure/taxonomy/manage/rank/overview add the ranks as your requirements.

5. Go to admin/structure/block block layout to place the level bar in the page

6. Go to user/1/points/applied to add some points, and then execute the points transaction to watch your level and the level bar.




